Drop TABLE friendship
Drop Table reptile
CREATE TABLE friendship (
    reptile_1  VARCHAR2(100) NOT NULL,
    reptile_2  VARCHAR2(100) NOT NULL
);

ALTER TABLE friendship ADD CONSTRAINT friendship_pk PRIMARY KEY ( reptile_1,
                                                                  reptile_2 );

CREATE TABLE reptile (
    reptileid VARCHAR2(100) NOT NULL
);

ALTER TABLE reptile ADD CONSTRAINT reptile_pk PRIMARY KEY ( reptileid );

ALTER TABLE friendship
    ADD CONSTRAINT friendship_reptile_fk1 FOREIGN KEY ( reptile_1 )
        REFERENCES reptile ( reptileid );

ALTER TABLE friendship
    ADD CONSTRAINT friendship_reptile_fk2 FOREIGN KEY ( reptile_2 )
        REFERENCES reptile ( reptileid );


INSERT INTO REPTILE VALUES ('TheLizardOfRANGO');
INSERT INTO REPTILE VALUES ('MasterViperFromKungFuPanda');
INSERT INTO REPTILE VALUES ('TheLizardOfGEICO');

Insert into friendship values
(
'TheLizardOfGEICO',
'TheLizardOfRANGO'
)
Insert into friendship values
(
'TheLizardOfGEICO',
'MasterViperFromKungFuPanda'
)

select * from REPTILE

select * from friendship