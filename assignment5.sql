/*Question 1: Some statistical models used in predictive analytics (such as neural networks) are sensitive to the magnitude of numerical values. 
A common approach when building such models is to normalize a numeric attribute so that all of its values are inside the range [0, 1]. 
Keep in mind that statistical normalization is not the same as database normalization, and this question deals with the former. 
For example, an attribute Age with five instances containing the values 1, 3, 5, 32, 102, would have the following values after normalization: 0, 0.0198, 0.0396, 0.3069, 1.
a) Write a function NORMALIZATION that brings a set of values into the range [0, 1]. 
If you do not know how the statistical normalization process works, you should first research about it. 
For example, you can start with the query how to normalize values between 0 and 1 using any search engine. 
The NORMALIZATION function must have three arguments: a list of values, and the maximum and minimum values in that list. In other words, it should start as follows: [20 points]
*/
CREATE OR REPLACE FUNCTION NORMALIZATION (OriginalValues NUMBER,
MaxNumber NUMBER,
MinNumber NUMBER)
   RETURN NUMBER 
AS
BEGIN       
      RETURN((OriginalValues-MinNumber)/(MaxNumber-MinNumber)); 
    END;
    
/*Show that your function works. Specifically, create a table X with a single attribute called Age of type INTEGER. 
Insert 8 values into Age, namely 1, 3, 5, 32, 25, 102, 109, 11, and report a query that returns the original Age values alongside the normalized values. You are not allowed to use hard-coded values in your query. 
For example, instead of using 109 as MaxNumber, you should use the query SELECT MAX(Age) FROM X; as an argument of NORMALIZATION. [10 points]*/
  CREATE TABLE "AS5"."X" 
   (	"AGE" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into AS5.X
SET DEFINE OFF;
Insert into X values (1);
Insert into X values (3);
Insert into X values (5);
Insert into X values (32);
Insert into X values (25);
Insert into X values (102);
Insert into X values (109);
Insert into X values (11);

select NORMALIZATION(AGE,
(SELECT MAX(Age) FROM X),
(SELECT MIN(Age) FROM X)
) from X

/*Question 2: Reconsider the accounting-system example studied in class during Lecture 23, which is shown below:
Before solving the following questions, make sure you download the file A5Q2.sql available on Canvas and run all the statements in it to create the required tables, data, and trigger.
a) Write a procedure called TRANSFER, which has the following arguments: AccountIDFrom NUMBER, AccountIDTo NUMBER, TransferAmount NUMBER. The TRANSFER procedure consists of 4 operations:
1. A new transaction of type D (debit) is stored in the TRANSACTIONS table. The TransactionID of this transaction is equal to the previous TransactionID plus one. 
The AccountID of this transaction is AccountIDFrom; [5 points]
Page | 3
2. Update the balance of the account with AccountID equal to AccountIDFrom. The new balance is equal to the old balance minus TransferAmount; [5 points]
3. A new transaction of type C (credit) is stored in the TRANSACTIONS table. The TransactionID of this transaction is equal to the previous TransactionID plus one. The AccountID of this transaction is AccountIDTo; [5 points]
4. Update the balance of the account with AccountID equal to AccountIDTo. The new balance is equal to the old balance plus TransferAmount. [5 points]
*/
select * from Accounts;
select * from transactions;
CREATE OR REPLACE PROCEDURE TRANSFER
(AccountIDFrom NUMBER, AccountIDTo NUMBER, TransferAmount NUMBER)
IS
   BEGIN
   INSERT INTO transactions values (
   (select MAX(transactionID) + 1 from transactions),
   AccountIDFrom,
   'D',
   CURRENT_DATE
   );
   Update Accounts
   set AccountBalance = AccountBalance - TransferAmount
   where accountID = AccountIDFrom;
   
    INSERT INTO transactions values (
   (select MAX(transactionID) + 1 from transactions),
   AccountIDTo,
   'C',
   CURRENT_DATE
   );
   Update Accounts
   set AccountBalance = AccountBalance + TransferAmount
   where accountID = AccountIDTo;
   END;

/*Show that the TRANSFER procedure works. In particular, simulate a scenario where a person with AccountID equal to 1 transfers $10 to a person with AccountID equal to 2. 
To do so, show the resulting tables after running each one of the following commands: [5 points]*/   
CALL TRANSFER(1, 2, 10);
select * from Accounts;
select * from transactions;

/*Suppose that a fraud-detection system will use data from SIMPLE_LOG to try to detect and prevent fraudulent transactions. 
Due to an internal miscommunication, the fraud-detection system requires data about the transferred amount 
(NewAccountBalance  OldAccountBalance), 
as opposed to data about new balance (NewAccountBalance) or old balance (OldAccountBalance). Instead of redeveloping the whole fraud-detection system or a new database,
you realize that the above problem can be fixed by simply creating a VIEW on top of SIMPLE_LOG, 
called ADAPTED_LOG, that returns the four attributes required by the fraud-detection system, namely LogId, AccountId, TransferredAmount, and TransactionTimeStamp. Report the SQL code to create ADAPTED_LOG. [10 points]*/
select * from simple_log;
CREATE or replace VIEW ADAPTED_LOG AS
select 
LogId,
AccountId,
(NewAccountBalance - OldAccountBalance) as TransferredAmount,
TransactionTimeStamp
from simple_log;

/*Show that ADAPTED_LOG works as desired by showing the resulting tables after running each one of the following commands: [5 points]*/
SELECT * FROM ADAPTED_LOG;
CALL TRANSFER (3, 4, 50);
SELECT * FROM ADAPTED_LOG;