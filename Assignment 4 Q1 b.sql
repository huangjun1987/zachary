CREATE TABLE reptile (
    reptileid  VARCHAR2(100) NOT NULL,
    friendid   VARCHAR2(100)
);

ALTER TABLE reptile ADD CONSTRAINT reptile_pk PRIMARY KEY ( reptileid );

ALTER TABLE reptile
    ADD CONSTRAINT reptile_reptile_fk FOREIGN KEY ( friendid )
        REFERENCES reptile ( reptileid );

INSERT INTO REPTILE VALUES ('TheLizardOfRANGO', NULL);
INSERT INTO REPTILE VALUES ('MasterViperFromKungFuPanda', NULL);
Insert into reptile values
(
'TheLizardOfGEICO',
'TheLizardOfRANGO'
)
Insert into reptile values
(
'TheLizardOfGEICO',
'MasterViperFromKungFuPanda'
)